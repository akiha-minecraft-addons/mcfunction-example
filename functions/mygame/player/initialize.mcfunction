# プレイヤーの状態を初期化する

# 全ての状態異常を消す
effect @s clear
# HPを回復する
effect @s instant_health 1 255 true
# 満腹度を回復する
effect @s saturation 1 255 true
# 全ての持ち物を消す
clear @s
# ゲームモードをアドベンチャーにする
gamemode adventure @s
