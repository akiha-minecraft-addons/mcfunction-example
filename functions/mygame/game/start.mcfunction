# ゲームを開始する

# プレイヤーの状態を初期化する
function mygame/player/initialize

# 初期装備を与える
# 木の剣を持たせる
give @s wooden_sword
# 食べ物を与える
give @s bread 3

# ゲーム開始地点に移動
spreadplayers 0 0 5 20 @s
