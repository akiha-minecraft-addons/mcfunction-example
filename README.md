# Example function command

`/function`コマンドを使ってみるための参考ファイルです。

詳しい使い方は次のページを参考にしてください。

[https://mc.akihamitsuki.net/command/function/](https://mc.akihamitsuki.net/command/function/)
